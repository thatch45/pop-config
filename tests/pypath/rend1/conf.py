CLI_CONFIG = {
    "yaml": {
        "render": "yaml",
    },
    "json": {
        "render": "json",
    },
    "cli": {
        "render": "cli",
    },
}
CONFIG = {
    "yaml": {
        "default": {"rubber": "Svien"},
        "help": "A yaml rendered option",
    },
    "json": {
        "default": {"rubber": "Svien"},
        "help": "A json rendered option",
    },
    "cli": {
        "default": {"rubber": "Svien"},
        "help": "A cli rendered option",
    },
}
