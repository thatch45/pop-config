CONFIG = {
    "config": {
        "default": None,
        "help": "Load extra options from a .yaml configuration file onto hub.OPT",
    },
    "picker": {
        "dyne": "__cli__",
        "default": "enabled",
        "choices": ["disabled", "enabled", "strict"],
        "help": "Pick one of the options",
        "os": "PICKER",
    },
    "list_picker": {
        "dyne": "__cli__",
        "default": None,
        "help": "Pick one of the options",
        "os": "LIST_PICKER",
    },
}

CLI_CONFIG = {
    "config": {"options": ["-c"], "subcommands": ["_global_"]},
    "picker": {},
    "list_picker": {
        "nargs": "*",
        "choices": ["1", "2", "3"],
    },
}
