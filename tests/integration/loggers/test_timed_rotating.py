import datetime
import os
from typing import Any
from typing import Union

import pytest
from flaky import flaky


@pytest.mark.skipif(
    os.name == "nt", reason="This test does not run properly on windows"
)
@flaky(max_runs=5)
def test_timed_rotating(logfile, cli):
    cli(
        "--log-plugin=timed_rotating",
        "--log-level=trace",
        f"--log-file={logfile}",
        "--log-handler-options",
        "when=S",
        "interval=0.1",
        "backupCount=2",
    )

    rotated_log = next(logfile.parent.glob("*.log.*"))

    assert rotated_log.suffix.startswith(f".{datetime.datetime.now().year}")

    with open(rotated_log) as fh:
        contents = fh.read()

    # Verify that the current log file has contents
    assert "[TRACE   ] trace\n" in contents

    # Verify the backup
    contents: Union[Union[str, bytes], Any] = logfile.read_text()
    assert "[TRACE   ] trace\n" in contents
