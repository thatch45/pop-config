"""
A simple pop program to test config
"""
import copy
import json
import os
import unittest.mock as mock

import pop.hub

if __name__ == "__main__":
    hub = pop.hub.Hub()

    # Get args to this program from env
    load = json.loads(os.getenv("TEST_LOAD_STR"))
    euid = int(os.getenv("TEST_EUID", "0"))
    if euid:
        os.geteuid = mock.MagicMock(return_value=euid)

    hub.pop.sub.add(dyne_name="config")
    hub.config.integrate.load(*load)

    print(json.dumps(copy.copy(hub.OPT)))
