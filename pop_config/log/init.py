"""
This sub is used to set up logging for pop projects and injects logging
options into conf making it easy to add robust logging
"""
import logging
import sys
import inspect


class LogCleanup:
    """
    Provides a logging registry, tracking the log handlers added by the hub.

    This allows cleanup of hub log files. This is mainly useful in tests, where
    many hubs are created and deleted. Prior to this, some test suites were
    running out of file handles.
    """

    def __init__(self):
        self.handlers = []

    def addHandler(self, handler):
        self.handlers.append(handler)

    def __del__(self):
        for handler in self.handlers:
            logging.getLogger().removeHandler(handler)


def __init__(hub):
    """
    Set up variables used by the log subsystem
    """
    logging.addLevelName(5, "TRACE")
    hub.log.LEVEL = {
        "notset": logging.NOTSET,
        "trace": 5,
        "debug": logging.DEBUG,
        "info": logging.INFO,
        "warn": logging.WARN,
        "warning": logging.WARNING,
        "error": logging.ERROR,
        "fatal": logging.FATAL,
        "critical": logging.CRITICAL,
    }
    log = logging.getLogger(__name__)

    # These should be overwritten by the integrated logger, but here's a contingency
    hub.log.INT_LEVEL = log.getEffectiveLevel()
    hub.log.log = lambda level, msg, *args, **kwargs: hub.log.init.to_log(level, msg, *args, **kwargs)
    hub.log.trace = lambda msg, *args, **kwargs: log.log(5, msg, *args, **kwargs)
    hub.log.debug = lambda msg, *args, **kwargs: hub.log.init.to_log(logging.DEBUG, msg, *args, **kwargs)
    hub.log.info = lambda msg, *args, **kwargs: hub.log.init.to_log(logging.INFO, msg, *args, **kwargs)
    hub.log.critical = lambda msg, *args, **kwargs: hub.log.init.to_log(logging.CRITICAL, msg, *args, **kwargs)
    hub.log.warning = lambda msg, *args, **kwargs: hub.log.init.to_log(logging.WARNING, msg, *args, **kwargs)
    hub.log.error = lambda msg, *args, **kwargs: hub.log.init.to_log(logging.ERROR, msg, *args, **kwargs)

    # Provides a way for loggers to be cleaned up on hub deletion. This is mostly exercised in test suites.
    hub.log.handlers = LogCleanup()


def to_log(hub, level, msg, *args, **kwargs):
    if hasattr(sys, "_getframe"):
        fnum = 4
        while True:
            frame = sys._getframe(fnum)
            if "self" not in frame.f_locals:
                fnum +=1
                continue
            name = frame.f_locals["self"].ref
            break
    else:
        fnum = 4
        while True:
            frame = inspect.stack(0)[fnum][0]
            if "self" not in frame.f_locals:
                fnum += 1
                continue
            name = call_frame[0].f_locals["self"].ref
            break
    log = logging.getLogger(name)
    log.log(level, msg, *args, **kwargs)
